//
//  events.swift
//  testFW1
//
//  Created by EhabDev on 6/19/20.
//  Copyright © 2020 EhabDev. All rights reserved.
//

import UIKit
import Branch
/*
 
 BRANCH_STANDARD_EVENT.RATE
 BRANCH_STANDARD_EVENT.ADD_TO_CART
 BRANCH_STANDARD_EVENT.INITIATE_PURCHASE
 BRANCH_STANDARD_EVENT.SEARCH
 BRANCH_STANDARD_EVENT.VIEW_ITEM
 BRANCH_STANDARD_EVENT.PURCHASE
 BRANCH_STANDARD_EVENT.VIEW_CART
 BRANCH_STANDARD_EVENT.LOGIN
 BRANCH_STANDARD_EVENT.COMPLETE_REGISTRATION
 */
public class events: NSObject {
    
    public class func signIn(){
        API.userSession(type: .signIn)
        
        
        let event =  BranchEvent.standardEvent(.login)
        
        event.customData["package_name"] = helper.appID()
        event.customData["user_id"] = helper.userID()
        event.customData["time_stamp"] = Date.getTimeStamp()
        event.logEvent()
    }
    
    
    public class func signOut(){
        API.userSession(type: .signOut)
        
        let event =  BranchEvent.customEvent(withName: "sign_out")
        event.customData["package_name"] = helper.appID()
        event.customData["user_id"] = helper.userID()
        event.customData["time_stamp"] = Date.getTimeStamp()
        event.logEvent()
    }
    
    public class func signUp(){
        API.userSession(type: .signUp)
        
        let event =  BranchEvent.standardEvent(.completeRegistration)
        event.customData["package_name"] = helper.appID()
        event.customData["user_id"] = helper.userID()
        event.customData["time_stamp"] = Date.getTimeStamp()
        event.logEvent()
    }
    
    public class func luanchApp(){
        API.userSession(type: .luanchApp)
        
        let event =  BranchEvent.customEvent(withName: "app_launch")
        event.customData["package_name"] = helper.appID()
        event.customData["user_id"] = helper.userID()
        event.customData["time_stamp"] = Date.getTimeStamp()
        event.logEvent()
    }
    
    public class func promoRedeemed(promoCode: String,amount: Int){
        API.promoRedeem(promoCode: promoCode, amount: amount)
        
        let event =  BranchEvent.customEvent(withName: "promo_code_redeemed")
        event.customData["package_name"] = helper.appID()
        event.customData["user_id"] = helper.userID()
        event.customData["amount"] = "\(amount)"
        event.customData["promo_code"] = promoCode
        event.logEvent()
    }
    
    public class func search(query: String){
        API.searchItem(query: query)
        
        let event =  BranchEvent.standardEvent(.search)
        event.customData["package_name"] = helper.appID()
        event.customData["user_id"] = helper.userID()
        event.customData["query"] = query
        event.logEvent()
    }
    
    public class func viewItem(itemID: String){
        API.viewItem(itemID: itemID)
        
        let event =  BranchEvent.standardEvent(.viewItem)
        event.customData["package_name"] = helper.appID()
        event.customData["user_id"] = helper.userID()
        event.customData["item_id"] = itemID
        event.logEvent()
    }
    
    public class func addItem(itemID: String, cartID: String){
        API.addItem(cartID: cartID, itemID: itemID)
        
        let event =  BranchEvent.standardEvent(.addToCart)
        event.customData["package_name"] = helper.appID()
        event.customData["user_id"] = helper.userID()
        event.customData["item_id"] = itemID
        event.customData["basket_id"] = cartID
        event.logEvent()
    }
    
    public class func removeItemFromCart(itemID: String, cartID: String){
        API.removeItemFromCart(cartID: cartID, itemID: itemID)
        
        let event =  BranchEvent.customEvent(withName: "remove_from_cart")
        event.customData["package_name"] = helper.appID()
        event.customData["user_id"] = helper.userID()
        event.customData["item_id"] = itemID
        event.customData["basket_id"] = cartID
        event.logEvent()
    }
    
    public class func viewCart(cartID: String){
        API.viewCart(cartID: cartID)
        
        let event =  BranchEvent.standardEvent(.viewCart)
        event.customData["package_name"] = helper.appID()
        event.customData["user_id"] = helper.userID()
        event.customData["basket_id"] = cartID
        event.logEvent()
    }
    
    public class func transactionConfirmation(cartID: String, orderID: String){
        API.transaction(cartID: cartID, orderID: orderID)
        
        let event =  BranchEvent.standardEvent(.purchase)
        event.customData["package_name"] = helper.appID()
        event.customData["user_id"] = helper.userID()
        event.customData["basket_id"] = cartID
        event.customData["order_id"] = orderID
        event.logEvent()
    }
    
    public class func cartCheckout(item:checkOut){
        API.checkOut(item: item)
        
        let event =  BranchEvent.standardEvent(.initiatePurchase)
        event.customData["package_name"] = helper.appID()
        event.customData["user_id"] = helper.userID()
        event.customData["order_id"] = "\(item.orderID)"
        event.customData["items"] = "\(item.items)"
        event.customData["promo_code"] = item.promoCode
        event.customData["promo_discount_amount"] = "\(item.promoDiscountAmount)"
        event.logEvent()
    }
    
    public class func orderFeedback(feed:setfeedback){
        API.feedBack(item: feed.feedbackList, orderID: feed.orderID, note: feed.note)
        
        var x = [String(),Int()] as [Any]
        for i in feed.feedbackList {
            x.append(i.nsDictionary)
        }
        
        let event =  BranchEvent.standardEvent(.rate)
        event.customData["package_name"] = helper.appID()
        event.customData["user_id"] = helper.userID()
        event.customData["order_id"] = "\(feed.orderID)"
        event.customData["feedback_list"] = "\(x)"
        event.customData["note"] = feed.note
        
        event.logEvent()
    }
    
    
    //dsl
    //feedback >> rate
    //BranchEvent.standardEvent(.initiatePurchase) checkout
    
    
}
