//
//  consts.swift
//  testFW1
//
//  Created by EhabDev on 6/14/20.
//  Copyright © 2020 EhabDev. All rights reserved.
//

import Foundation
public enum userGender:Int{
    
    case male = 1
    case female = 2
}
public enum countrySwiftCode:String{
    
    case EG = "20"
    case SA = "966"
}

enum userSessionType:String{
    case signIn = "signIn"
    case signOut = "signOut"
    case signUp = "signUp"
    case luanchApp = "luanchApp"
}
public struct feedBack{
     var title = String()
      var rate = Int()
    
  public init(title: String, rate: Int) {
        self.title   = title
        self.rate = rate
    }
    
  public  var dictionary: [String: Any] {
        return [
            "title": title,
                "rate": rate
        ]
    }
   public var nsDictionary: NSDictionary {
        return dictionary as NSDictionary
    }
}
struct Links {
    static let mainUrl = "http://mobile-test.pin-offer.com/api/sdk/"
    
    /// post
    static let Auth = mainUrl + "auth_apikey"
    static let userData = mainUrl + "user_data"
    static let UserSession = mainUrl + "user_session"
    static let promoCode = mainUrl + "promo_redeemd"
    static let searchView = mainUrl + "search_view"
    static let addDeleteCart = mainUrl + "art_action"
    static let viewCart = mainUrl + "view_basket"
    static let transaction = mainUrl + "transaction_confirmation"
    static let checkOut = mainUrl + "cart_checkout"
    static let feedback = mainUrl + "feed_back"
}

public struct configAuth {
    static var apikey = ""
    static var secretkey = ""
}

func saveKey(value: String,key: String){
    let def=UserDefaults.standard
    def.setValue(value, forKey: key)
    def.synchronize()
}

func getKey(key: String) -> String{
    let def=UserDefaults.standard
    if def.object(forKey: key) == nil {
        return ""
    }else{
        return def.object(forKey: key) as! String
    }
}

public class configUserData {
    
    public var id : String
    public var gender : userGender
    public var countryCode : countrySwiftCode
    public var email : String
    /// ex: "1988-1-18"
    public var  birthday : String
    public var name : String
    public var fcmToken : String
    
    /*
     gender = userGender(rawValue: 0)!
     countryCode = countrySwiftCode(rawValue: "")!
     */
    public init() {
        gender = .male
        id = ""
        countryCode = .EG
        email = ""
        birthday = ""
        name = ""
        fcmToken = ""
    }
}

public class checkOut {
    public var orderID : Int
    public var items : [Int]
    public var promoCode : String
    public var  promoDiscountAmount : Double
    
    public init(){
        orderID = 0
        items = []
        promoCode = ""
        promoDiscountAmount = 0.0
    }
    
}

public class setfeedback: NSObject {
    
    public var orderID : Int = 10
    public var note : String = ""
    public var feedbackList = [feedBack]()
    
    
    
}
