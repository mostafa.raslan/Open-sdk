//
//  helper.swift
//  testFW1
//
//  Created by EhabDev on 6/14/20.
//  Copyright © 2020 EhabDev. All rights reserved.
//

import Foundation
import UIKit

class helper: NSObject {
    
    // save user Token
    class func token(token: String){
        saveKey(value: token, key: "userToken")
    }
    
    // save Application ID
    class func appID(bundelID: String){
        saveKey(value: bundelID, key: "MyBundelID")
    }
    
    // save User ID (Mobile Number)
    class func userID(userID: String){
        saveKey(value: userID, key: "userID")
    }
    
   
}

extension helper {
    // get token
       class func token() -> String{
           getKey(key: "userToken")
       }
       
       // get Application ID
       class func appID() -> String{
           getKey(key: "MyBundelID")
           
       }
    
    // get User ID ID
    class func userID() -> String{
        getKey(key: "userID")
        
    }
}

