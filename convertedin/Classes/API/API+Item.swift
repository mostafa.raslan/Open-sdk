//
//  API+Item.swift
//  testFW1
//
//  Created by EhabDev on 6/19/20.
//  Copyright © 2020 EhabDev. All rights reserved.
//

import Foundation

extension API {
    
    class func searchItem(query: String){
        let param:[String: Any ] = [
            "action": "search",
            "query": query
        ]
        print("SearchItem param >> \(param)")
        
        request.requestPost(url: Links.searchView, param: param){(status,event) in
            if status{
                print("call api SearchItem is Done \(String(describing: event))")
            }else{
                print("call api SearchItem is fill")
            }
        } // end reqiest SearchItem end point
        
    }// end SearchItem class
    
    class func viewItem(itemID: String){
           let param:[String: Any ] = [
               "action": "view",
               "item_id": itemID
           ]
           print("viewItem param >> \(param)")
           
           request.requestPost(url: Links.searchView, param: param){(status,event) in
               if status{
                   print("call api viewItem is Done \(String(describing: event))")
               }else{
                   print("call api viewItem is fill")
               }
           } // end reqiest viewItem end point
        
       }// end viewItem class
    
   
}
