//
//  API+Amount.swift
//  testFW1
//
//  Created by EhabDev on 6/19/20.
//  Copyright © 2020 EhabDev. All rights reserved.
//

import Foundation

extension API {
    
    class func promoRedeem(promoCode: String,amount: Int){
        let param:[String: Any ] = [
            "amount": "\(amount)",
            "promo_code": promoCode
        ]
        print("promoRedeem param >> \(param)")
        
        request.requestPost(url: Links.promoCode, param: param){(status,event) in
            if status{
                print("call api promoRedeem is Done \(String(describing: event))")
            }else{
                print("call api promoRedeem is fill")
            }
        } // end reqiest promo redeem end point
    }// end promCode class
    
   
}
