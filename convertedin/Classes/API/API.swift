//
//  API.swift
//  testFW1
//
//  Created by EhabDev on 6/13/20.
//  Copyright © 2020 EhabDev. All rights reserved.
//

import UIKit
import Foundation

class API : NSObject {
    
    class func getAuth(){
        BranchConfig.config()
        let param:[String: Any ] = [
            "apikey":pinofferInit.apikey,
            "secretkey":pinofferInit.secretKey
        ]
        
        helper.appID(bundelID: pinofferInit.ApplicationID)
        request.requestPost(url: Links.Auth, param: param){(status,event) in
            if status{
                helper.token(token: event!["token"] as! String)
                print("xx:\(helper.token())\n user is Auth: \(String(describing: event))")
            }else{
                print("call api Auth is fill")
            }
        } // end reqiest get Auth end point
    }
    
    class func setUserData(item: configUserData){
        
        let param:[String: Any ] = [
            "id": item.id,
            "countryCode": item.countryCode.rawValue,
            "name": item.name,
            "birthday": item.birthday,
            "gender": item.gender.rawValue,
            "fcm_token":item.fcmToken,
            "email": item.email,
            /*"extraData":[
             "key1":"data2",
             "key2":"data1"
             ]*/
            
        ]
        
        request.requestPost(url: Links.userData, param: param){(status,event) in
            if status {
                let userID = "\(item.countryCode.rawValue)\(item.id)"
                helper.userID(userID: userID)
                print("userid after register:\(helper.userID(userID: userID))")
                Utill.printx("user is registerd: \(String(describing: event))")
            }else{
                Utill.printx("user is not register")
            }
        }// end request set user data end point
    }
    
    
    /*
         {
         "note": "زى الفل",
         "order_id":2020,
         "feedback_list": [
         {
         "title": "place",
         "rate": 4
         },
         {
         "title": "sales",
         "rate": 5
         }
         ]
         }
     */
    
    class func feedBack(item: [feedBack], orderID: Int, note: String){
        var x = [String(),Int()] as [Any]
        for i in item {
            x.append(i.nsDictionary)
        }
        let param:[String: Any ] = [
            "note": note,
            "order_id":orderID,
            "feedback_list": x
        ]
        print("feedBack param >> \(param)")
        request.requestPost(url: Links.feedback, param: param){(status,event) in
            if status {
                
                Utill.printx("user is feedBack: \(String(describing: event))")
            }else{
                Utill.printx("user is not feedBack")
            }
        }// end request set feedBack end point
    }
}

extension Encodable {

    /// Converting object to postable dictionary
    func toDictionary(_ encoder: JSONEncoder = JSONEncoder()) throws -> [String: Any] {
        let data = try encoder.encode(self)
        let object = try JSONSerialization.jsonObject(with: data)
        guard let json = object as? [String: Any] else {
            let context = DecodingError.Context(codingPath: [], debugDescription: "Deserialized object is not a dictionary")
            throw DecodingError.typeMismatch(type(of: object), context)
        }
        return json
    }
}
