//
//  request.swift
//  testFW1
//
//  Created by EhabDev on 6/13/20.
//  Copyright © 2020 EhabDev. All rights reserved.
//

import UIKit
import Foundation

class request: NSObject {
    
    class func requestPost(url: String,param: [String: Any] = [:],_ completion: @escaping (Bool,[String:Any]?) -> Void){
        //print("url:\(url)")
        let session = URLSession.shared
        let url = URL(string: url)!
        var request = URLRequest(url: url)
         let param:[String: Any ] = param
        
        print("url:\(url)")
         print("Auth : \(helper.token())")
        request.httpMethod = "POST"
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.setValue("application/json", forHTTPHeaderField: "Accept")
       
        if helper.token() != "" {
            print("user Phone id from header: \(helper.userID())")
            request.setValue("Bearer "+helper.token(), forHTTPHeaderField: "Authorization")
            request.setValue(helper.userID(), forHTTPHeaderField: "id")
        }
        
       print(param)
        request.httpBody = try! JSONSerialization.data(withJSONObject: param, options: [])
        
        let task = session.dataTask(with: request) { data, response, error in
          
            
            if error != nil || data == nil {
                Utill.printx("Client error!")
                completion(false,[:])
                return
            }
            
            guard let response = response as? HTTPURLResponse, (200...299).contains(response.statusCode) else {
                Utill.printx("Server error!")
                  completion(false,[:])
                return
            }
            
            guard let mime = response.mimeType, mime == "application/json" else {
                Utill.printx("Wrong MIME type!")
                 completion(false,[:])
                return
            }
            
            do {
                let json = try JSONSerialization.jsonObject(with: data!, options: []) as! [String:Any]
                Utill.printx("done:\(json)")
                completion(true,json)
            } catch {
                Utill.printx("JSON error: \(error.localizedDescription)")
                completion(false,[:])
            }
        }
        
        task.resume()
    }

}
