//
//  API+Cart.swift
//  testFW1
//
//  Created by EhabDev on 6/19/20.
//  Copyright © 2020 EhabDev. All rights reserved.
//

import Foundation
extension API {
    
    class func addItem(cartID: String, itemID: String){
        let param:[String: Any ] = [
            "action": "add",
            "item_id": itemID,
            "basket_id" : cartID
        ]
        print("addItem param >> \(param)")
        
        request.requestPost(url: Links.addDeleteCart, param: param){(status,event) in
            if status{
                print("call api addItem is Done \(String(describing: event))")
            }else{
                print("call api addItem is fill")
            }
        } // end reqiest addItem end point
        
    }// end addItem class
    
    class func removeItemFromCart(cartID: String, itemID: String){
        let param:[String: Any ] = [
            "action": "remove",
            "item_id": itemID,
            "basket_id" : cartID
        ]
        print("removeItemFromCart param >> \(param)")
        
        request.requestPost(url: Links.addDeleteCart, param: param){(status,event) in
            if status{
                print("call api removeItemFromCart is Done \(String(describing: event))")
            }else{
                print("call api removeItemFromCart is fill")
            }
        } // end reqiest removeItemFromCart end point
        
    }// end removeItemFromCart class
    
    class func viewCart(cartID: String){
           let param:[String: Any ] = [
               "basket_id" : cartID
           ]
           print("viewCart param >> \(param)")
           
           request.requestPost(url: Links.viewCart, param: param){(status,event) in
               if status{
                   print("call api viewCart is Done \(String(describing: event))")
               }else{
                   print("call api viewCart is fill")
               }
           } // end reqiest viewCart end point
           
       }// end viewCart class
    
    class func transaction(cartID: String, orderID: String){
              let param:[String: Any ] = [
                  "basket_id" : cartID,
                  "order_id" : orderID
              ]
              print("transactionConfirmation param >> \(param)")
              
              request.requestPost(url: Links.transaction, param: param){(status,event) in
                  if status{
                      print("call api transactionConfirmation is Done \(String(describing: event))")
                  }else{
                      print("call api transactionConfirmation is fill")
                  }
              } // end reqiest transactionConfirmation end point
              
          }// end transactionConfirmation class
    
    class func checkOut(item:checkOut){
                 let param:[String: Any ] = [
                    "order_id": item.orderID,
                    "items":item.items ,
                    "promo_code": item.promoCode,
                    "promo_discount_amount": item.promoDiscountAmount
                 ]
                 print("checkOut param >> \(param)")
                 
                 request.requestPost(url: Links.checkOut, param: param){(status,event) in
                     if status{
                         print("call api checkOut is Done \(String(describing: event))")
                     }else{
                         print("call api checkOut is fill")
                     }
                 } // end reqiest checkOut end point
                 
             }// end checkOut class
}
