//
//  Date+Extension.swift
//  testFW1
//
//  Created by EhabDev on 6/19/20.
//  Copyright © 2020 EhabDev. All rights reserved.
//

import Foundation
import UIKit

extension Date{
  static  func getDateString() -> String{
        let date = Date()
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        formatter.string(from: date)
        return  formatter.string(from: date)// 2020-06-19
    }
    
   static func getHourString() -> String{
        let date = Date()
        let formatter = DateFormatter()
        formatter.dateFormat = "hh:mm:ss a"
        formatter.string(from: date)
        return  formatter.string(from: date)// 04:00:33 PM
    }
    
    static func getTimeStamp() -> String{
        return "\(getDateString()) \(getHourString())"
    }
    
}
