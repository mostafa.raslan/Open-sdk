#
# Be sure to run `pod lib lint convertedin.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see https://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'convertedin'
  s.version          = '0.1.2'
  s.summary          = 'An SDK for Consumer Analytics and Ads Platform for Brick and Mortar Retailers'

# This description is used to generate tags and improve search results.
#   * Think: What does it do? Why did you write it? What is the focus?
#   * Try to keep it short, snappy and to the point.
#   * Write the description between the DESC delimiters below.
#   * Finally, don't worry about the indent, CocoaPods strips it!

  s.description      = <<-DESC
  'Converted.In SDK will help physical and offline retailers understand their customers in and out of store behavior and boost their sales through effective offline to online re-targeting.'
                         DESC

  s.homepage         = 'https://developer.converted.in'
  # s.screenshots     = 'www.example.com/screenshots_1', 'www.example.com/screenshots_2'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'convertedin' => 'Info@converted.in' }
  s.source           = { :git => 'https://gitlab.com/mostafa.raslan/Open-sdk.git', :tag => s.version.to_s }
  s.social_media_url = 'https://twitter.com/Convertedin'

  s.ios.deployment_target = '13.0'
  s.swift_version = '5.0'

  s.source_files = 'convertedin/Classes/**/*'
  
  # s.resource_bundles = {
  #   'convertedin' => ['convertedin/Assets/*.png']
  # }

  # s.public_header_files = 'Pod/Classes/**/*.h'
  # s.frameworks = 'UIKit', 'MapKit'
  s.dependency 'Branch'
end
