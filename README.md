# Convertedin

[![CI Status](https://img.shields.io/travis/devkhaled/convertedin.svg?style=flat)](https://travis-ci.org/devkhaled/convertedin)
[![Version](https://img.shields.io/cocoapods/v/convertedin.svg?style=flat)](https://cocoapods.org/pods/convertedin)
[![License](https://img.shields.io/cocoapods/l/convertedin.svg?style=flat)](https://cocoapods.org/pods/convertedin)
[![Platform](https://img.shields.io/cocoapods/p/convertedin.svg?style=flat)](https://cocoapods.org/pods/convertedin)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

convertedin is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'convertedin'
```

## Author

convertedin, Info@converted.in

## License

convertedin is available under the MIT license. See the LICENSE file for more info.
